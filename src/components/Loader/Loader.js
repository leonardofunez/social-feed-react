import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './Loader.scss'

const Loader = props => {
  return (
    props.is_loading ? 
      <div className='loader'>
        <div className='loader__content f-weight-600 f-12'>
          <div className='loader__svg'></div>
        </div>
      </div>
    :
      null
  );
}

Loader.propTypes = {
  is_loading : PropTypes.bool.isRequired
}

const mapStateToProps = state => {
  return {
    is_loading: state.is_loading
  }
}

export default connect(mapStateToProps)(Loader)