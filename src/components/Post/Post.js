import React, { useState, useEffect } from 'react'
import ProfilePhoto from '../ProfilePhoto/ProfilePhoto'

import store from '../../redux/store'
import { FAVORITES } from '../../redux/creators'

import './Post.scss'

const Post = props => {
  const [like, setLike] = useState(false)

  const convertDate = (inputFormat) => {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    const d = new Date(inputFormat)
    const dateString = `${monthNames[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()}`
    
    return dateString
  }

  const stringToURL = (value) => {
    if(value){
      const exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      const text = value.replace(exp, "<a href='$1' target='_blank'>$1</a>")
      return <span dangerouslySetInnerHTML={{__html: text}} />
    }
  }

  const isFavorite = () => {
    const favoritesStore = store.getState().favorites
    setLike( favoritesStore.filter(item => item.id === props.id).length === 1 ? true : false )
  }

  const likeTweet = () => {
    let favoritesStore = store.getState().favorites
    
    const newFavorite = {
      id: props.id,
      user_url: props.userUrl,
      user_image: props.userImage,
      user_name: props.userName,
      created: props.created,
      text: props.text
    }

    if( favoritesStore.filter(item => item.id === props.id).length === 0 ){
      store.dispatch(FAVORITES([...favoritesStore, newFavorite]))
    }else{
      let indexItem = favoritesStore.findIndex( item => item.id === props.id)
      favoritesStore.splice(indexItem, 1)
      store.dispatch(FAVORITES(favoritesStore))
    }
    
    setLike(!like)

    if( props.likeAction ){
      props.likeAction(props.id)
    }
  }

  useEffect(() => {
    isFavorite()
  }, [])
  
  return(
    <article className='post'>
      <div className='post__header'>
        <a href={`https://twitter.com/${props.userUrl}`} target='_blank' rel='noopener noreferrer'>
          <ProfilePhoto photo={props.userImage} dimension='40' />
        </a>

        <div className='post__actions'>
          <div className='post__action post__like' data-active={'true' ? like : 'false'} onClick={() => likeTweet()}></div>
        </div>
      </div>

      <h2 className='post__name'>
        <a href={`https://twitter.com/${props.userUrl}`} target='_blank' rel='noopener noreferrer'>{props.userName}</a>
      </h2>
      <div className='post__date'>{convertDate(props.created)}</div>
      <p className='post__text'>{stringToURL(props.text)}</p>
    </article>
  )
}

export default Post