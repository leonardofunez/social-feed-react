import React from 'react'
import Logo from '../../assets/img/logo.svg'

import './Footer.scss'

const Footer = () => {
  return(
    <footer className='main-footer'>
      <div className='wrapper'>
        <img className='main-footer__logo' src={Logo} alt='Social Feed' width='30' />
        <div className='main-footer__by'>Designed and Developed by <a href='http://leonardofunez.com' target='_blank' rel='noopener noreferrer'>Leonardo Funez</a></div>
      </div>
    </footer>
  )
}

export default Footer