import React from 'react'
import { NavLink } from 'react-router-dom'
import ProfilePhoto from '../../components/ProfilePhoto/ProfilePhoto'

import Photo from '../../assets/img/andrea_pirlo.jpg'
import HomeIcon from '../../assets/img/home.svg'
import FavoriteIcon from '../../assets/img/favorites.svg'
import MenuIcon from '../../assets/img/menu.svg'

import './Header.scss'

const Header = () => {
  return(
    <header className='main-header'>
      <NavLink to={'/'} className='main-header__logo'></NavLink>
      
      <div className='main-header__menu'>
        <NavLink to={'/'} className='main-header__menu-item' activeClassName="main-header__menu-item--active">
          <img className='main-header__menu-icon' src={HomeIcon} alt='Home' width='20' />Home
        </NavLink>
        <NavLink to={'/favorites'} className='main-header__menu-item' activeClassName="main-header__menu-item--active">
          <img className='main-header__menu-icon' src={FavoriteIcon} alt='Favorites' width='20' />Favorites
        </NavLink>

        <div className='main-header__user'>
          <ProfilePhoto photo={Photo} dimension='40' />
          <img className='main-header__user-menu' src={MenuIcon} alt='Favorites' width='16' />
        </div>
      </div>
    </header>
  )
}

export default Header