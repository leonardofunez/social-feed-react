import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'

import Loader from '../components/Loader/Loader'

import Home from '../pages/Home'
import Favorites from '../pages/Favorites'

const routes = (
  <Router>
    <React.Fragment>
      <main className='main'>
        <Route path='/' component={Header} />

        <Loader />
        
        <Route path='/' component={Home} exact />
        <Route path='/favorites' component={Favorites} exact />
        
        <Route path='/' component={Footer} />
      </main>
    </React.Fragment>
  </Router>
)

export default routes