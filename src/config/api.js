import axios from 'axios'

const baseURL = `http://api.massrelevance.com`

export default {
  async getPosts() {
    try {
      const response = await axios.get(`${baseURL}/MassRelDemo/kindle.json`)
      return await response.data
    }
    catch (error) {
      console.log(error)
    }
  },
}