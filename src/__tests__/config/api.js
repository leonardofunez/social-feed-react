import API from '../../config/api'

describe('API', () => {
  it('Returns Posts', async (done) => {
    const response = await API.getPosts()
    expect(response.length).toBeGreaterThan(0)
    done()
  })
})