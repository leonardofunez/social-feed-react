import { IS_LOADING, FAVORITES } from '../../redux/creators'
import configureStore from 'redux-mock-store'

const mockStore = configureStore()

describe('Action Creators', () => {
  it('Update the loading state', () => {
    const store = mockStore({ is_loading: false })

    const newLoadingState = true
    store.dispatch(IS_LOADING(newLoadingState))

    const actions = store.getActions()
    expect(actions[0].is_loading).toEqual(true)
    expect(actions[0].type).toBe('IS_LOADING')
    expect(actions[0].is_loading).not.toBeNull()
  })


  it('Add favorite posts', () => {
    const store = mockStore({ favorites: [] })

    const newPost = {
      id: '11111',
      user_url: 'http://t.co/users/1111',
      user_image: 'http://t.co/users/11111/image.jpg',
      user_name: 'Leonardo Funez',
      created: 'Dec 2, 2019',
      text: 'Lorem ipsum dolor sit amet'
    }

    store.dispatch(FAVORITES(newPost))

    const actions = store.getActions()
    expect(actions[0].favorites).toEqual(newPost)
    expect(actions[0].type).toBe('FAVORITES')
    expect(actions[0].favorites).not.toBeNull()
  })
})