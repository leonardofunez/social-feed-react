import { is_loading as IS_LOADING, favorites as FAVORITES } from '../../redux/reducers'

describe('Reducers', () => {
  it('Loading returns the inital state', () => {
    expect(IS_LOADING(undefined, false)).toEqual(false);
  })

  it('Loading receives the new loading state', () => {
    const is_loading = true
    expect(IS_LOADING(false, {type: 'IS_LOADING', is_loading})).toEqual(is_loading)
  })

  it('Favorite posts returns the inital state', () => {
    expect(FAVORITES(undefined, {})).toEqual([]);
  })
  it('Favorite posts receives the post list', () => {
    const favorites = [
      {
        id: '11111',
        user_url: 'http://t.co/users/1111',
        user_image: 'http://t.co/users/11111/image.jpg',
        user_name: 'Leonardo Funez',
        created: 'Dec 2, 2019',
        text: 'Lorem ipsum dolor sit amet'
      },
      {
        id: '22222',
        user_url: 'http://t.co/users/2222',
        user_image: 'http://t.co/users/2222/image.jpg',
        user_name: 'Steve Joel',
        created: 'Oct 2, 2019',
        text: 'Lorem ipsum dolor sit amet'
      }
    ]
    expect(FAVORITES([], {type: 'FAVORITES', favorites})).toEqual(favorites)
  })
})