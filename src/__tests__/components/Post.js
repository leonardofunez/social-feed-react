import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, mount, configure } from 'enzyme'
import Post from '../../components/Post/Post'

configure({adapter: new Adapter()})

describe('<Post />', () => {
  it('Renders the user name', () => {
    const wrapper = shallow(<Post userName='Leonardo Funez' />)
    expect(wrapper.find('.post__name a').length).toBe(1)
  })

  it('Renders the creation date', () => {
    const created = 'Fri Dec 29 19:15:04 +0000 2017'
    const wrapper = shallow(<Post created={created} />)
    expect(wrapper.find('.post__date').length).toBe(1)
  })

  it('Renders the text / description', () => {
    const text = 'The battle is life or death, but that doesn’t mean love plays no part.'
    const wrapper = shallow(<Post text={text} />)
    expect(wrapper.find('.post__date').length).toBe(1)
  })

  it('Changes like state', () => {
    const wrapper = mount(<Router><Post /></Router>)
    wrapper.find('.post__like').simulate('click')
    expect(wrapper.find('.post__like').prop('data-active')).toBe(true)
  })
});