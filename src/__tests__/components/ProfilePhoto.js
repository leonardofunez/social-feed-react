import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import ProfilePhoto from '../../components/ProfilePhoto/ProfilePhoto'

configure({adapter: new Adapter()})

describe('<ProfilePhoto />', () => {
  it('Renders the photo title', () => {
    const photo = 'http://photo.path.jpg'
    const wrapper = shallow(<ProfilePhoto photo={photo} />)
    expect(wrapper.prop('style').background).toBe(`url(${photo}) no-repeat center / cover`)
  })
})