import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { mount, configure } from 'enzyme'
import configureStore from 'redux-mock-store'
import Loader from '../../components/Loader/Loader'

configure({adapter: new Adapter()})
const mockStore = configureStore()

describe('<Loader />', () => {
  it('Renders no content', () => {
    const store = mockStore({ is_loading: false })
    const wrapper = mount(<Loader store={store} />)
    expect(wrapper.find('.loader__content').length).toBe(0)
  })

  it('Renders content', () => {
    const store = mockStore({ is_loading: true })
    const wrapper = mount(<Loader store={store} />)
    expect(wrapper.find('.loader__content').length).toBe(1)
  })
});