import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'

import store from '../redux/store'
import { IS_LOADING } from '../redux/creators'

import Post from '../components/Post/Post'

const Favorites = () => {
  const [posts, setPosts] = useState([]),
        [isLoading, setIsLoading] = useState([])

  const getPosts = () => {
    setPosts(store.getState().favorites)

    setTimeout( () => {
      store.dispatch(IS_LOADING(false))
      setIsLoading(false)
    }, 1500);
  }

  const likeAction = (tweet_id) => {
    let newPosts = posts.filter( post => post.id !== tweet_id )

    setPosts([])
    setPosts(newPosts)
  }

  useEffect(() => {
    store.dispatch(IS_LOADING(true))
    setIsLoading(true)

    getPosts()
  }, [])

  return(
    <section className='container'>
      <div className='wrapper'>
        <div className='posts-list'>
          {
            !isLoading ?
              posts.length > 0 ?
                posts.map((post, index) =>
                  <Post
                    key={`${index}-${post.id}`}
                    id={post.id}
                    text={post.text}
                    created={post.created}
                    userName={post.user_name}
                    userImage={post.user_image}
                    userUrl={post.user_url}
                    likeAction={likeAction}
                  />
                )
              :
                <div className='posts-list__no-post'>
                  <h2 className='posts-list__no-post-text'>You have no favorite posts!</h2>
                  <NavLink to='/' className='button'>Back to Home</NavLink>
                </div>
            : null
          }
        </div>
      </div>
    </section>
  )
}

export default Favorites