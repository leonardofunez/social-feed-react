import React, { useState, useEffect } from 'react'
import SocialAPI from '../config/api'

import store from '../redux/store'
import { IS_LOADING } from '../redux/creators'

import Post from '../components/Post/Post'

const Home = () => {
  const [posts, setPosts] = useState([]),
        [isLoading, setIsLoading] = useState([])

  const getPosts = async () => {
    const response = await SocialAPI.getPosts()
    setPosts(response)

    setTimeout( () => {
      store.dispatch(IS_LOADING(false))
      setIsLoading(false)
    }, 1500);
  }

  useEffect(() => {
    store.dispatch(IS_LOADING(true))
    setIsLoading(true)

    getPosts()

    return(() => {
      store.dispatch(IS_LOADING(false))
      setIsLoading(false)
    })
  }, [])

  return(
    <section className='container'>
      <div className='wrapper'>
        <div className='posts-list'>
          {
            !isLoading ?
              posts.map((post, index) =>
                <Post
                  key={`${index}-${post.id}`}
                  id={post.id}
                  network={post.network}
                  text={post.text}
                  created={post.created_at}
                  userName={post.user.name}
                  userImage={post.user.profile_image_url}
                  userUrl={post.user.screen_name}
                />
              )
            : null
          }
        </div>
      </div>
    </section>
  )
}

export default Home