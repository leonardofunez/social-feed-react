export const IS_LOADING = state => {
  return {
    type: 'IS_LOADING',
    is_loading: state
  }
}

export const FAVORITES = items => {
  return {
    type: 'FAVORITES',
    favorites: items
  }
}