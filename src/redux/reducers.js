export const is_loading = (state = false, action) => {
  if (action.type === 'IS_LOADING') return action.is_loading

  return state
}

export const favorites = (state = [], action) => {
  if (action.type === 'FAVORITES') return action.favorites

  return state
}